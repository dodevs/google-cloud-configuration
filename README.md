# Google Cloud Configuration

Coisas para configurar meus projetos do google cloud

## Criar projeto

### Configurar Firewall

Portas: 
* WEB: 80, 443
* SSH: 22
* sqlserver: 1433
* postgres: 5432
* pgadmin: 5050
* portainer: 9000

Ip de origem: 177.154.162.0/24. Se aplica a:
* portainer
* SSH
* pgadmin

### Configurar rede VPC

Habiliar ip externo statico para a instancia

### Configurar acesso ao projeto

Dar uma olhada nisso https://cloud.google.com/compute/docs/instances/managing-instance-access?hl=pt-br

## Instancia GCE

* Tipo da maquina: e2-standard-2 (2 vCPUs, 8gb memoria)
* Zona: southamerica-east1-b
* Imagem: debian-9-stretch
* Disco: 500Gb SSD ou Permanente
* Nome: e2vm-ubuntu

### Pós instalação

#### Configurar Docker

instalar o docker engine https://docs.docker.com/install/linux/docker-ce/debian/
instalar o docker-compose
configurar acesso sem sudo

#### Containers

##### SqlServer

* <b>Comando</b>

> docker run -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=<password>' -e 'MSSQL_PID=Developer' -p 1433:1433 --name=sqlserver -v sqlserver_data:/var/opt/mssql -d mcr.microsoft.com/mssql/server:2019-latest

* <b>Dicas</b>

enviar para dentro do container:
> docker cp file <container_name>:/var/opt/mssql/backup

listar nomes logicos e caminhos para a base a ser restaurada:
> sudo docker exec -it sql1 /opt/mssql-tools/bin/sqlcmd -S localhost \
   -U SA -P '<YourNewStrong!Passw0rd>' \
   -Q 'RESTORE FILELISTONLY FROM DISK = "/var/opt/mssql/backup/wwi.bak"' \
   | tr -s ' ' | cut -d ' ' -f 1-2

restaurar base nos lugares encontrados:
> sudo docker exec -it sql1 /opt/mssql-tools/bin/sqlcmd \
   -S localhost -U SA -P '<YourNewStrong!Passw0rd>' \
   -Q 'RESTORE DATABASE WideWorldImporters FROM DISK = "/var/opt/mssql/backup/wwi.bak" WITH MOVE "WWI_Primary" TO "/var/opt/mssql/data/WideWorldImporters.mdf", MOVE "WWI_UserData" TO "/var/opt/mssql/data/WideWorldImporters_userdata.ndf", MOVE "WWI_Log" TO "/var/opt/mssql/data/WideWorldImporters.ldf", MOVE "WWI_InMemory_Data_1" TO "/var/opt/mssql/data/WideWorldImporters_InMemory_Data_1"'

verificar se foram restaurados:
> sudo docker exec -it sql1 /opt/mssql-tools/bin/sqlcmd \
   -S localhost -U SA -P '<YourNewStrong!Passw0rd>' \
   -Q 'SELECT Name FROM sys.Databases'

##### gitlab-runner

https://docs.gitlab.com/runner/install/docker.html

Run container
> docker run -d --name gitlab-runner --restart always -p 8093:8093 -v /srv/gitlab-runner/config:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:latest

Registro do runnable
> docker run --rm -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register \
  --non-interactive \
  --executor "docker" \
  --docker-image alpine:latest \
  --url "https://gitlab.com/" \
  --registration-token "<project_token>" \
  --description "docker-runner" \
  --tag-list "gce-instance" \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected" \
  --docker-privileged

##### portainer

https://www.portainer.io/installation/

##### Postgres e pg4admin

https://github.com/dodevs/Postgres_Docker


#### Aprender sobre o Cloud Storage

Como criar buckets e acessá-los de dentro das instancias:
> gsutil cp gs://[BUCKET_NAME]/[OBJECT_NAME] [SAVE_TO_LOCATION]

Alternativa:

> enviar arquivo para a instancia: gcloud compute scp <file> <instancia>:<[path/]filename>

#### Dicas gerais de dev

* <b>NUNCA USE O GOOGLE APPLICATION ENGINE NO MESMO PROJETO</b> essa porra nao apaga
* defina o template0 no ecto para nao ter erro com o banco de dados


